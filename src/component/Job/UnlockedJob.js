/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import {False, If, True} from '../stateless/If';
import {Mce} from '../stateless/Mce';
import {Collapse, CollapseContent, CollapseLabel} from '../stateless/Collapse';
import {DatePicker} from '../stateless/DatePicker';
import {SkillSelector} from '../Skill/SkillSelector';
import {RemoveButton} from '../stateless/RemoveButton';
import {FileHelper} from '../../lib/FileHelper';
import {filterSelectableSkills, filterSelectedSkills} from '../Skill/repository/SkillRepository';

/**
 * Component for Job in locked mode
 */
export class UnlockedJob {
    /**
     * @param {Component}   component
     * @param {Actions}     component['attrs'].actions
     * @param {Number}      component['attrs']['state'].createdAt
     */
    constructor({ 'attrs': { actions, 'state': { createdAt } }}){
        this.loadIcon = ({ target }) => {
            if (target.files.length > 0){
                let file = target.files[0];
                FileHelper.fileToDataUrl(file)
                    .then((dataUrl) => {
                        actions.dispatch(actions.job.changeIcon(createdAt, dataUrl));
                        m.redraw();
                    });
            }
        };
        this.resetIcon = () => actions.dispatch(actions.job.resetIcon(createdAt));
    }

    /**
     * @param   {Component}       component
     * @param   {Actions}         component['attrs'].actions
     * @param   {Number}          component['attrs'].index
     * @param   {Array<Skill>}    component['attrs'].availableSkills
     * @param   {Job}             component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': {actions, index, availableSkills, 'state': { title, begin, end, description, createdAt, skills, icon }}}){
        const selectableSkills = availableSkills.filter(filterSelectableSkills(createdAt)(skills)),
              selectedSkills = availableSkills.filter(filterSelectedSkills(createdAt)(skills));
        return (
          <div>
            <If condition={index === 0}>
                <True>
                    <div className="timeline-item">
                        <button className="button is-medium is-success" onclick={actions.delegate(() => actions.job.add((end || begin)+1000))} >
                            <i className="fa fa-plus-circle"/>
                        </button>
                    </div>
                </True>
            </If>
            <div className="timeline-item">
                <div className="timeline-marker is-primary"></div>
                <div className="timeline-content is-full">
                    <p className="heading">
                        <div className="is-divider" data-content={ title }></div>
                        <div>
                            <button className="button is-medium is-danger" onclick={actions.delegate(() => actions.job.remove(createdAt))} >
                                <i className="fa fa-minus-circle"/>
                            </button>
                        </div>
                    </p>
                    <div className="field">
                        <div className="control has-icons-left">
                            <input
                                className="input"
                                oninput={actions.event((value) => actions.job.changeTitle(createdAt, value))}
                                value={ title }
                                placeholder="Titre"
                            />
                            <span className="icon is-small is-left">
                                <i className="fa fa-fw fa-building" />
                            </span>
                        </div>
                    </div>
                    <div className="field is-grouped">
                        <div className="control has-icons-left is-expanded">
                            <DatePicker
                                oninput={actions.event((value) => actions.job.changeBegin(createdAt, value.format('x')))}
                                placeholder="Date de début"
                                value={begin}
                            />
                            <span className="icon is-small is-left">
                                <i className="fa fa-fw fa-fast-backward" />
                            </span>
                        </div>
                        <div className="control has-icons-left is-expanded">
                            <DatePicker
                                oninput={actions.event((value) => actions.job.changeEnd(createdAt, value.format('x')))}
                                placeholder="Date de fin"
                                value={end}
                            />
                            <span className="icon is-small is-left">
                                <i className="fa fa-fw fa-fast-forward" />
                            </span>
                        </div>
                    </div>
                    <div className="field">
                        <label className="label"><i className="fa fa-building-o"/> Description succinte </label>
                        <div className="control">
                            <div className="control">
                                <Collapse>
                                    <CollapseLabel>
                                        <div className="button is-fullwidth is-medium is-info is-outlined ">Description</div>
                                    </CollapseLabel>
                                    <CollapseContent>
                                        <Mce oninput={actions.event((value) => actions.job.changeDescription(createdAt, value))}>
                                            { description }
                                        </Mce>
                                    </CollapseContent>
                                </Collapse>
                            </div>
                        </div>
                    </div>
                    <div className="is-divider" data-content="Compétences associées" ></div>
                    {selectedSkills
                        .map((skill) => (
                                <p key={skill.createdAt} className="panel-block">
                                    <RemoveButton onclick={actions.event(() => actions.job.changeRemoveSkill(createdAt, skill.createdAt), true)}>
                                        {skill.title}
                                    </RemoveButton>
                                </p>
                            )
                        )}
                    <If condition={selectableSkills.length > 0}>
                        <True>
                            <div className="panel-block">
                                <SkillSelector selectableSkills={selectableSkills} onclick={({ target }) => {
                                    if (target.value !== null){
                                        actions.dispatch(actions.job.changeAddSkill(createdAt, Number(target.value)));
                                    }
                                }}/>
                            </div>
                        </True>
                    </If>
                    <div className="file is-boxed is-fullwidth">
                        <label className="file-label">
                            <input className="file-input" onchange={this.loadIcon} type="file"/>
                            <span className="file-cta">
                                <If condition={Boolean(icon)}>
                                    <True>
                                        <img src={ icon } />
                                        <RemoveButton onclick={this.resetIcon} />
                                    </True>
                                    <False>
                                          Icone
                                          <span className="file-icon">
                                            <i className="fa fa-upload" />
                                          </span>
                                    </False>
                                </If>
                            </span>
                        </label>
                    </div>
                </div>
            </div>
            <div className="timeline-item">
                <button className="button  is-success" onclick={actions.delegate(() => actions.job.add(begin-1000))} >
                    <i className="fa fa-plus-circle"/>
                </button>
            </div>
        </div>
      );
    }
}

export default UnlockedJob;
