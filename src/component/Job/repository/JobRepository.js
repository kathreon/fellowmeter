/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * @param   {Number} selectedSkill
 * @returns {function(Job): Job}
 */
export const filterBySkill = (selectedSkill) => {
    return (job) => selectedSkill
        ? job.skills.find((skillCreated) => skillCreated === selectedSkill)
        : job;
};

/**
 * @param {Job} a
 * @param {Job} b
 * @returns {Number}
 */
export const sortDescendantBegin = (a, b) => {
    return Number(b.begin) - Number(a.begin);
};
