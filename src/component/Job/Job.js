/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import { LockedJob } from './LockedJob';
import { UnlockedJob } from './UnlockedJob';
import { If, True, False } from '../stateless/If';

import { filterBySkill, sortDescendantBegin } from './repository/JobRepository';

/**
 * Job Main Component
 * @property {GetStateCallback} getState
 */
export class Job {
    /**
     * @param {Component} component
     * @param {Provider}  component['attrs'].provider
     */
    constructor({ 'attrs': { provider }}){
        this.getState = provider.provide(({ job = [], skill = [], 'application': { locked, selectedSkill = null } }) => ({ 'jobs': job, 'skills': skill, locked, selectedSkill }));
    }

    /**
     * @param {Component} component
     * @param {Actions}   component['attrs'].actions
     * @returns {Component}
     */
    view({ 'attrs': { actions }}){
        let { locked, jobs, selectedSkill, skills } = this.getState();
        return (
          <div>
            <div className="timeline">
                <If condition={jobs.length === 0 && !locked}>
                    <True>
                        <div className="timeline-item">
                            <button className="button is-medium is-success" onclick={actions.delegate(() => actions.job.add(null))} >
                                <i className="fa fa-plus-circle"/>
                            </button>
                        </div>
                    </True>
                </If>
                <If condition={locked}>
                    <True>
                        {
                            jobs.filter(filterBySkill(selectedSkill))
                                .sort(sortDescendantBegin)
                                .map((state) => <LockedJob state={state} key={state.createdAt} />)
                        }
                    </True>
                    <False>
                        {
                            jobs
                                .sort(sortDescendantBegin)
                                .map((state, index) => <UnlockedJob state={state} actions={actions} key={state.createdAt} index={index} availableSkills={skills} />)
                        }
                    </False>
                </If>
            </div>
        </div>
      );
    }
}

export default Job;
