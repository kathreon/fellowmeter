/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import { Markdown } from '../stateless/Markdown';
import { If, True } from '../stateless/If';
import moment from 'moment';

/**
 * Component for Job in locked mode
 */
export class LockedJob {
    /**
     * @param {Component} component
     * @param {Job}       component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': { 'state': { title, begin, end, description, icon } } }){
        return (
          <div className="timeline-item">
            <div className="timeline-marker is-primary"></div>
            <div className="timeline-content">
                <p className="heading">
                    <div className="is-divider" data-content={ title }></div>
                </p>
                <p className="heading">
                    <i className="fa fa-fw fa-fast-backward" />
                    &nbsp;{moment(begin).format('DD/MM/YYYY')} au
                    &nbsp;<i className="fa fa-fw fa-fast-forward" />
                    &nbsp;{moment(end).format('DD/MM/YYYY')}
                </p>
                <p className="content">
                    <p className="content">
                        <Markdown markdown={description} />
                    </p>
                    <If condition={Boolean(icon)}>
                        <True>
                            <p>
                                <img src={icon}/>
                            </p>
                        </True>
                    </If>
                </p>
            </div>
        </div>
      );
    }
}
export default LockedJob;
