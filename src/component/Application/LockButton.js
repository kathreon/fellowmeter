/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { If, True, False } from '../stateless/If';

/**
 * A button Component able to switch between "edit" and "view" modes.
 */
export class LockButton {
    /**
     * @param {Component}   component
     * @param {Boolean}     component['attrs'].locked      - current mode
     * @param {function()}  component['attrs'].lock        - switch to locked/view mode
     * @param {function()}  component['attrs'].unlock      - switch to unlocked/edit mode
     * @returns {Component}
     */
    view({ 'attrs': { locked = false, lock, unlock }}){
        return (
          <If condition={locked}>
            <True>
                <button className="button level-item is-outlined is-large is-dark" onclick={unlock} title="unlock">
                    <i className="fa fa-fw fa-unlock"/>
                    <span className="is-hidden-touch">
                        Unlock edition
                    </span>
                </button>
            </True>
            <False>
                <button className="button level-item is-outlined is-large is-dark" onclick={lock} title="lock">
                    <i className="fa fa-fw fa-lock"/>
                    <span className="is-hidden-touch">
                        Lock edition
                    </span>
                </button>
            </False>
          </If>
        );
    }
}
export default LockButton;
