/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import './style/bulma.sass';
import 'font-awesome/css/font-awesome.min.css';

import { Contact } from '../Contact/Contact';
import { Skill } from '../Skill/Skill';
import { Job } from '../Job/Job';

import { LockButton } from './LockButton';
import { Slide, Slider } from '../stateless/Slide';
import { ConfirmButton } from '../stateless/ConfirmButton';


/**
 * Application Root Component
 * @property {Persister}   persister
 * @property {Provider}    provider
 * @property {Actions}     actions
 * @property {GetStateCallback}          getState
 * @property {ActionDispatcherCallback}  lock
 * @property {ActionDispatcherCallback}  unlock
 * @property {ActionDispatcherCallback}  reset
 * @property {function()}                saveConfig
 * @property {function()}                cloneConfig
 * @property {function(Event)}           loadConfig
 */
export class Application {
    /**
     * @param {Component}   component
     * @param {Provider}    component['attrs'].provider
     * @param {Actions}     component['attrs'].actions
     * @param {Persister}   component['attrs'].persister
     */
    constructor({ 'attrs': { provider, actions, persister } }){
        this.persister = persister;
        this.provider = provider;
        this.actions = actions;
        this.getState = this.provider.provide(
            ({ 'contact': { name, title }, 'application': { locked, modifiedAt }}) => ({ locked, name, title, modifiedAt })
        );
        this.lock = actions.delegate(actions.application.lock);
        this.unlock = actions.delegate(actions.application.unlock);
        this.saveConfig = () => this.persister.downloadStateAsJson(this.provider.getState());
        this.cloneApplication = () => this.persister.downloadStateAsHtml(this.provider.getState());
        this.loadConfig = ({ target }) => {
            if (target.files.length > 0){
                this.persister.getFileState(target.files[0])
                    .then((state) => {
                        actions.dispatch(actions.application.loadState(state));
                        m.redraw();
                    });
            }
        };
    }

    /** @returns {Component} */
    view(){
        let { locked, name, title } = this.getState();

        return (
            <div>
                <nav className="navbar hero is-fixed-top">
                    <div className="level is-active">
                            <LockButton
                                locked={locked}
                                lock={this.lock}
                                unlock={this.unlock}
                            />
                            <button className="button level-item is-outlined is-large is-dark" onclick={this.cloneApplication} title="clone">
                                <i className="fa fa-fw fa-clone"/>
                                <span className="is-hidden-touch">
                                    Clone
                                </span>
                            </button>
                            <ConfirmButton onConfirm={() => this.actions.dispatch(this.actions.application.reset())}>
                              <button className="button level-item is-outlined is-large is-dark" title="erase">
                                <i className="fa fa-fw fa-eraser"/>
                                <span className="is-hidden-touch">
                                  Erase
                                </span>
                              </button>
                            </ConfirmButton>
                            <button className="button level-item is-outlined is-large is-dark" onclick={this.saveConfig} title="download config">
                                <i className="fa fa-fw fa-download"/>
                                <span className="is-hidden-touch">
                                    Download config
                                </span>
                            </button>
                            <div className="button file is-outlined is-large is-dark">
                                <label className="file-label level-item " title="use configuration file">
                                    <input className="file-input" type="file" onchange={this.loadConfig} />
                                    <div className="is-transparent">
                                      <span className="">
                                        <i className="fa fa-fw fa-upload" />
                                          <span className="is-hidden-touch">
                                            Upload config
                                          </span>
                                      </span>
                                    </div>
                                </label>
                            </div>

                    </div>
                </nav>
                <nav className="navbar is-dark">
                    <div className="navbar-brand">
                        <div className="navbar-item">
                            <h1 className="title has-text-light is-size-6-mobile">
                                {name}{title.trim() === '' ? '' : ' - '+title}
                            </h1>
                        </div>
                    </div>
                </nav>
                <Slider>
                    <Slide key={0}>
                        <Contact provider={this.provider} actions={this.actions} />
                    </Slide>
                    <Slide key={1}>
                        <Skill provider={this.provider} actions={this.actions} />
                    </Slide>
                    <Slide key={2}>
                        <Job provider={this.provider} actions={this.actions} />
                    </Slide>
                </Slider>
        </div>
      );
    }
}
export default Application;
