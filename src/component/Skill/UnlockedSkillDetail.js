/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import {RemoveButton} from '../stateless/RemoveButton';
import {SkillSelector} from './SkillSelector';
import {Mce} from '../stateless/Mce';
import {Collapse, CollapseContent, CollapseLabel} from '../stateless/Collapse';
import {False, If, True} from '../stateless/If';
import {FileHelper} from '../../lib/FileHelper';
import {filterSelectableSkills, filterSelectedSkills} from './repository/SkillRepository';

/**
 * Component for Skill details in unlocked mode
 */
export class UnlockedSkillDetail {
    /**
     * @param {Component} component
     * @param {Actions}   component['attrs'].actions
     * @param {Number}    component['attrs'].createdAt
     */
    constructor({ 'attrs': { actions, 'state': { createdAt } } }){
        this.current = createdAt;
        this.loadIcon = ({ target }) => {
            if (target.files.length > 0){
                let file = target.files[0];
                FileHelper.fileToDataUrl(file)
                    .then((dataUrl) => {
                        actions.dispatch(actions.skill.changeIcon(this.current, dataUrl));
                        m.redraw();
                    });
            }
        };
        this.resetIcon = () => actions.dispatch(actions.skill.resetIcon(createdAt));
    }

    /**
     * @param {Component} component
     * @param {Number}    component['attrs']['state'].createdAt
     */
    onupdate({ 'attrs': { 'state': { createdAt } } }){
        this.current = createdAt;
    }

    /**
     * @param {Component}       component
     * @param {Actions}         component['attrs'].actions
     * @param {Array<Skill>}    component['attrs'].availableSkills
     * @param {Skill}           component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': { actions, availableSkills, 'state': { title, description, context, createdAt, skills, icon } } }){
        const selectableSkills = availableSkills.filter(filterSelectableSkills(createdAt)(skills)),
              selectedSkills = availableSkills.filter(filterSelectedSkills(createdAt)(skills));

        return (
          <div className="message">
            <div className="message-header">
                <p>{ title }</p>
                <button onclick={() => actions.dispatch(actions.skill.reset())} class="delete" />
            </div>
            <div className="message-content">
                <div className="columns">
                    <nav className="panel column" >
                        <div className="panel-block">
                            <div className="control has-icons-left">
                                <input
                                    className="input is-fullwidth"
                                    oninput={actions.event((value) => actions.skill.changeTitle(createdAt, value), true)}
                                    value={ title }
                                    placeholder="Titre"
                                />
                                <span className="icon is-small is-left">
                                    <i className="fa fa-fw fa-building" />
                                </span>
                            </div>
                        </div>
                        <div className="is-divider" data-content="Compétences associées" ></div>
                        {selectedSkills
                            .map((skill) => (
                                    <p key={skill.createdAt} className="panel-block">
                                        <RemoveButton onclick={actions.event(() => actions.skill.changeRemoveSkill(createdAt, skill.createdAt), true)}>
                                            {skill.title}
                                        </RemoveButton>
                                    </p>
                                )
                            )}
                        <If condition={selectableSkills.length > 0}>
                            <True>
                                <div className="panel-block">
                                    <SkillSelector selectableSkills={selectableSkills} onclick={({ target }) => {
                                        if (target.value !== null){
                                            actions.dispatch(actions.skill.changeAddSkill(createdAt, Number(target.value)));
                                        }
                                    }}/>
                                </div>
                            </True>
                        </If>
                    </nav>
                    <div className="column">
                        <div className="field">
                            <div className="control">
                                <Collapse>
                                    <CollapseLabel>
                                        <div className="button is-fullwidth is-medium is-info is-outlined ">Description</div>
                                    </CollapseLabel>
                                    <CollapseContent>
                                        <Mce force={this.current && this.current !== createdAt} oninput={actions.event((value) => actions.skill.changeDescription(createdAt, value))}>
                                            { description }
                                        </Mce>
                                    </CollapseContent>
                                </Collapse>
                            </div>
                        </div>
                        <div className="field">
                            <div className="control">
                                <Collapse>
                                    <CollapseLabel>
                                        <div className="button is-fullwidth is-medium is-info is-outlined ">Contexte</div>
                                    </CollapseLabel>
                                    <CollapseContent>
                                        <Mce force={this.current && this.current !== createdAt} oninput={actions.event((value) => actions.skill.changeContext(createdAt, value))}>
                                            { context }
                                        </Mce>
                                    </CollapseContent>
                                </Collapse>
                            </div>
                        </div>
                        <div className="file is-boxed is-fullwidth">
                            <label className="file-label">
                                <input className="file-input" onchange={this.loadIcon} type="file"/>
                                <span className="file-cta">
                                    <If condition={Boolean(icon)}>
                                        <True>
                                            <img src={ icon } />
                                            <RemoveButton onclick={this.resetIcon} />
                                        </True>
                                        <False>
                                              Icone
                                              <span className="file-icon">
                                                <i className="fa fa-upload" />
                                              </span>
                                        </False>
                                    </If>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      );
    }
}
export default UnlockedSkillDetail;
