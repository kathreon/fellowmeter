/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { Markdown } from '../stateless/Markdown';
import { If, True } from '../stateless/If';
import { filterSelectedSkills } from './repository/SkillRepository';

/**
 * Component for Skill details in locked mode
 */
export class LockedSkillDetail {
    /**
     * @param   {Component}       component
     * @param   {Actions}         component['attrs'].actions
     * @param   {Array<Skill>}    component['attrs'].availableSkills
     * @param   {Skill}           component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': { actions, availableSkills, 'state': { title, description, context, createdAt, skills, icon } } }){
        const selectedSkills = availableSkills.filter(filterSelectedSkills(createdAt)(skills));

        return (
          <div className="message">
            <div className="message-header">
                <p>{ title }</p>
                <button onclick={() => actions.dispatch(actions.skill.reset())} class="delete"></button>
            </div>
            <div className="message-content content">
                <If condition={Boolean(icon)}>
                    <True>
                        <p>
                            <img src={icon}/>
                        </p>
                    </True>
                </If>
                <If condition={description.trim() !== ''}>
                    <True>
                        <p>
                            <Markdown markdown={ description } />
                        </p>
                    </True>
                </If>
                <If condition={context.trim() !== ''}>
                    <True>
                        <p>
                            <Markdown markdown={ context } />
                        </p>
                    </True>
                </If>
                <If condition={selectedSkills.length > 0}>
                    <True>
                        <h5>Compétences liées</h5>
                        <ul>
                            {selectedSkills.map((skill) =>
                                <li>{skill.title}</li>
                            )}
                        </ul>
                    </True>
                </If>
            </div>
        </div>
      );
    }
}
export default LockedSkillDetail;
