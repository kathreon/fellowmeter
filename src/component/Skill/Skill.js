/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { If, True, False } from '../stateless/If';
import { SkillItem } from './SkillItem';
import { UnlockedSkillDetail } from './UnlockedSkillDetail';
import { LockedSkillDetail } from './LockedSkillDetail';

/**
 * Skill Main Component
 * @property {GetStateCallback} getState
 * @property {GetStateCallback} getSelectedSkill
 */
export class Skill {
    /**
     * @param {Component} component
     * @param {Provider}  component['attrs'].provider
     */
    constructor({ 'attrs': { provider }}){
        this.getState = provider.provide(
            ({ skill = [], 'application': { locked } }) => ({'skills': skill, locked})
        );
        this.getSelectedSkill = provider.provide(
            ({ skill = [], 'application': { selectedSkill } }) =>
                skill.find(
                    ({ createdAt }) => selectedSkill === createdAt
                ) || null
        );
    }

    /**
     * @param   {Component} component
     * @param   {Actions}   component['attrs'].actions
     * @returns {Component}
     */
    view({ 'attrs': { actions }}){
        let { locked, skills } = this.getState(),
            selectedSkill = this.getSelectedSkill();

        return (
          <div>
            <div className="hero is-fullheight">
                <div className="hero-body">
                    <div className="container">
                        <If condition={selectedSkill !== null}>
                            <True>
                                <If condition={locked}>
                                    <True>
                                        <LockedSkillDetail state={selectedSkill} actions={actions} availableSkills={skills} />
                                    </True>
                                    <False>
                                        <UnlockedSkillDetail state={selectedSkill} actions={actions} availableSkills={skills} />
                                    </False>
                                </If>
                            </True>
                        </If>
                        <div className="columns is-centered is-multiline">
                            {skills.map((state, index) =>
                                <SkillItem
                                    actions={actions}
                                    selected={selectedSkill}
                                    state={state}
                                    key={state.createdAt}
                                    index={index}
                                    locked={locked}
                                />
                            )}
                            <If condition={!locked}>
                                <True>
                                    <div className="column is-1">
                                        <button onclick={() => actions.dispatch(actions.skill.add())} className="button is-success">
                                            <i className="fa fa-plus" />
                                        </button>
                                    </div>
                                </True>
                            </If>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      );
    }
}

export default Skill;
