/**
 * @param {Number}  currentCreatedAt
 * @returns {function(Array<Number>): function(Skill)}
 */
export const filterSelectableSkills = (currentCreatedAt) => (skills) => (skill) => {
    return skills.indexOf(skill.createdAt) === -1 && skill.createdAt !== currentCreatedAt;
};

/**
 * @param {Number}  currentCreatedAt
 * @returns {function(Array<Number>): function(Skill)}
 */
export const filterSelectedSkills = (currentCreatedAt) => (skills) => (skill) => {
    return skills.indexOf(skill.createdAt) !== -1 && skill.createdAt !== currentCreatedAt;
};
