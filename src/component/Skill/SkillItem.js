/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import {If, True, False } from '../stateless/If';
import noImage from './assets/100x100.png';
import './style/style.sass';
import { ConfirmButton } from '../stateless/ConfirmButton';

/**
 * @param {Number} scrollDuration
 */
const scrollToTop = (scrollDuration) => {
    const scrollHeight = window.scrollY,
        scrollStep = Math.PI / (scrollDuration / 15),
        cosParameter = scrollHeight / 2;
    let scrollCount = 0,
        scrollMargin,
        scrollInterval = setInterval(() => {
            if(document.documentElement.scrollTop !== 0){
                scrollCount = scrollCount + 1;
                scrollMargin = cosParameter - (cosParameter * Math.cos(scrollCount * scrollStep));
                document.documentElement.scrollTop = scrollHeight - scrollMargin;
            } else {
                clearInterval(scrollInterval);
            }
        }, 15);
};

/**
 *
 * @param {Boolean} isSelected
 * @param {Actions} actions
 * @param {Number}  createdAt
 */
const selectClick = (isSelected, actions, createdAt) => {
    if(isSelected){
        actions.dispatch(actions.skill.reset());
    } else{
        actions.dispatch(actions.skill.select(createdAt));
    }
    scrollToTop(500);

};

/**
 * @param {Boolean} isSelected
 * @param {Boolean} isRelated
 * @returns {String}
 */
const computeGlowingClass = (isSelected, isRelated) => {
    if(isSelected){
        return ' glowing-strong';
    }
    if(isRelated){
        return ' glowing';
    }
    return '';
};

/**
 * Component for display a skill preview in a list
 * It displays on both locked & unlocked modes
 */
export class SkillItem {
    /**
     * @params  {Component} component
     * @params  {Actions}   component['attrs'].actions
     * @params  {Skill}     component['attrs'].selected
     * @params  {Boolean}   component['attrs'].locked
     * @params  {Skill}     component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': { actions, selected, locked, 'state': { title, createdAt, icon } } }){
        let isSelected = selected && selected.createdAt === createdAt,
            isRelated = selected && selected.skills.find((skillCreated) => skillCreated === createdAt);

        return (
          <div className={'column is-2 min-height-150px'}>
            <div className={'card min-height-100p' + computeGlowingClass(isSelected, isRelated)} onclick={() => selectClick(isSelected, actions, createdAt)}>
                <If condition={Boolean(icon)}>
                    <True>
                        <div className="card-image">
                            <figure className="image is-4by3">
                                <img src={ icon }/>
                            </figure>
                        </div>
                    </True>
                    <False>
                        <div className="card-image">
                            <figure className="image is-4by3">
                                <img src={ noImage }/>
                            </figure>
                        </div>
                    </False>
                </If>
                <div className="card-header">
                    <div className="card-header-title is-centered">{title}</div>
                </div>
                <div className="skill-card-control">
                  <If condition={!locked}>
                    <True>
                      <ConfirmButton onclick={(event) => event.stopPropagation()} onConfirm={() => {
                          actions.dispatch(actions.skill.remove(createdAt));
                        }
                      }>
                        <button className="button is-danger">
                          <i className="fa fa-trash" />
                        </button>
                      </ConfirmButton>
                    </True>
                  </If>
                </div>
            </div>
        </div>
      );
    }
}
export default SkillItem;
