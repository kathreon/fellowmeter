/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { SkillOption } from './SkillOption';
import { ListSelect } from '../stateless/ListSelect';

/**
 * Style Component for skill selection
 */
export class SkillSelector {
    /**
     * @param {Component}    component
     * @param {Array<Skill>} component['attr'].selectableSkills
     * @param {function()}   component['attr'].onclick
     * @returns {Component}
     */
    view({ 'attrs': {selectableSkills, onclick} }){
        return (
            <ListSelect
                onclick={onclick}
                placeholder={'Associer une compétence'}
            >
                {selectableSkills.map((skill) => (
                    <SkillOption key={skill.createdAt} skill={skill} />
                ))}
            </ListSelect>
        );
    }
}

export default SkillSelector;
