/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

/**
 * Component which represents an option element filled by Skill data
 */
export class SkillOption {
    /**
     * @param {Component}   component
     * @param {Skill}       component['attrs'].skill
     * @returns {Component}
     */
    view({ 'attrs': {skill} }){
        return (
          <option value={skill.createdAt}>
            {skill.title}
        </option>
      );
    }
}

export default SkillOption;
