/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

/**
 * Components which wrap sub-elements into a remove button
 */
export class RemoveButton {
    /**
     * @param {Component}        component
     * @param {function(Event)}  component['attrs'].onclick
     * @param {Array<Component>} component.children
     * @returns {Component}
     */
    view({ 'attrs': {onclick}, children }){
        return (
          <button className="button is-fullwidth" onclick={onclick}>
            <i className="fa fa-fw fa-times" />
            { children }
        </button>
      );
    }
}

export default RemoveButton;
