/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

/**
 * Generic Component for display stylish selects
 */
export class ListSelect {
    /**
     * @param {Component}        component
     * @param {Array<Component>} component.children
     * @param {function(Event)}  component['attrs'].onclick
     * @param {*}                component['attrs'].value
     * @param {String}           component['attrs'].placeholder
     * @returns {Component}
     */
    view({ 'attrs': {onclick, value, placeholder = false}, children }){
        return (
          <div className="control has-icons-left">
            <div className="select is-success is-fullwidth">
                <select value={value} onclick={onclick}>
                    {placeholder
                        ? (<option value={value}>
                            { placeholder }
                        </option>)
                        : ''
                    }
                    { children }
                </select>
            </div>
            <span className="icon is-small is-left">
              <i className="fa fa-plus" />
            </span>
        </div>
      );
    }
}

export default ListSelect;
