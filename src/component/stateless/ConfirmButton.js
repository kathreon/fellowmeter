/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
Copyright 2017 Damien Joris
*/

import m from 'mithril';

import swal from 'sweetalert2';
import { If, True, False } from '../stateless/If';

const sweetAlert = (onConfirm, onCancel) => {
  swal({
    'title':                'Are you sure?',
    'text':                 'You won\'t be able to revert this!',
    'type':                 'warning',
    'showCancelButton':     true,
    'confirmButtonColor':   '#3085d6',
    'cancelButtonColor':    '#d33',
    'confirmButtonText':    'Yes, delete all!',
    'cancelButtonText':     'No, cancel!',
    'confirmButtonClass':   'btn btn-success',
    'cancelButtonClass':    'btn btn-danger',
    'buttonsStyling':       true,
    'reverseButtons':       true
  }).then((result) => {
    if(result.value){
      onConfirm();
      m.redraw();
    } else if (onCancel !== null){
      onCancel();
    }
  });
};

export class ConfirmButton {
  view({ 'attrs': {onConfirm, onCancel = null, onclick = null}, children }){
    return (
      <div onclick={(event) => {
        if (onclick !== null){
          onclick(event);
        }
        sweetAlert(onConfirm, onCancel);
      }
      }>
        <If condition={children.length > 0}>
          <True>
            { children }
          </True>
          <False>
            <button>Confirmer</button>
          </False>
        </If>
      </div>
    );
  }
}
