import m from 'mithril';
import { Raw } from './Raw';
import marked from 'marked';

/**
 * @see https://github.com/chjj/marked#options-1
 */
marked.setOptions({
    'renderer':    new marked.Renderer(),
    'gfm':         true,
    'tables':      true,
    'breaks':      false,
    'pedantic':    false,
    'sanitize':    false,
    'smartLists':  true,
    'smartypants': false
});

/**
 * Helper Component for display a markdown string as HTML
 */
export class Markdown {
    /**
     * @param {Component} component
     * @param {String}    component['attrs'].markdown
     * @returns {(Component|String)}
     */
    view({ 'attrs': { markdown = '' } }){
        return markdown === '' ? '' : <Raw html={marked(markdown)} />;
    }
}

export default Markdown;
