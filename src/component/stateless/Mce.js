/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import SimpleMDE from 'simplemde';
import 'simplemde/dist/simplemde.min.css';
import './style/simplemde.css';


/** @see https://github.com/sparksuite/simplemde-markdown-editor#configuration */
const configuration = {
    'autoDownloadFontAwesome': false,
    'spellChecker':            false
};

/**
 * Generic Component which display a Markdown Editor handled by SimpleMDE
 * @property {SimpleMDE}                simplemde
 * @property {(function(Event)|null)}   oninput
 * @property {function(Event)}          onChange
 */
export class Mce {
    /**
     * @see https://mithril.js.org/lifecycle-methods.html#oncreate
     * @param {Component}       component
     * @param {Element}         component.dom
     * @param {function(Event)} component['attrs'].oninput
     */
    oncreate({ dom, 'attrs': { oninput }}){
        this.simplemde = new SimpleMDE({ 'element': dom, ...configuration });
        this.oninput = oninput;
        this.onChange = () => {
            if (this.oninput !== null){
                this.oninput({ 'target': { 'value': this.simplemde.value() }});
            }
        };
        this.simplemde.codemirror.on('change', () => this.onChange());
    }

    /**
     * @see https://mithril.js.org/lifecycle-methods.html#onupdate
     * @param {Component}        component
     * @param {Array<Component>} component.children
     * @param {Boolean}          component['attrs'].force
     * @param {function(Event)}  component['attrs'].oninput
     */
    onupdate({ children, 'attrs': { force = false, oninput }}){
        if (force){
            this.oninput = null;
            this.simplemde.value(children.map(({ 'children' : subChildren }) => subChildren? subChildren : '').join(''));
            this.oninput = oninput;
        }
    }

    /**
     * @see https://mithril.js.org/lifecycle-methods.html#onremove
     * @param {Component}        component
     * @param {Element}          component.dom
     */
    onremove({ dom }){
        dom.parentElement.querySelectorAll('*').forEach((element) => {
            element.remove();
        });
        delete this.simplemde;
    }

    /**
     * @param {Component}        component
     * @param {Array<Component>} component.children
     * @returns {Component}
     */
    view({ children }){
        return (
          <textarea
            style="overflow:auto;resize:none"
            className="textarea mce"
          >
            { children }
          </textarea>
        );
    }
}
export default Mce;
