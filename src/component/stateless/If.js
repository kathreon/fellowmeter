/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2018 François Cadeillan
*/

export const True = { 'view': ({ children }) => children };
export const False = { 'view': ({ children }) => children };

/**
 * Helper Component for easy-to-use ternary conditions
 */
export class If {
    /**
     * @param {Component}           component
     * @param {Boolean}             component['attrs'].condition
     * @param {Boolean}             component['attrs'].other
     * @param {Array<Component>}    component.children
     */
    view({ 'attrs': { condition = false, other = true }, children }){
      return children
               .filter((child) => {
                     switch (child.tag){
                       case True:
                         return condition;
                       case False:
                         return !condition;
                       default:
                         return other;
                     }
                   }
               );
    }
}

export default If;
