/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import Pikaday from 'pikaday';
import moment from 'moment';

import './style/pikaday.css';

const datePickerConfig = {
    'format': 'DD/MM/YYYY',
    'i18n':   {
        'previousMonth': 'Mois précédent',
        'nextMonth':     'Mois prochain',
        'months':        ['Janvier', 'Février', 'Mars', 'April', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
        'weekdays':      ['Dimanche', 'Lundi', 'Mardi', 'Mecredi', 'Jeudi', 'Vendredi', 'Samedi'],
        'weekdaysShort': ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam']
    }
};

/**
 * Generic Component for display date/time picker input field
 */
export class DatePicker {
    /**
     * @see https://mithril.js.org/lifecycle-methods.html#oncreate
     * @param {Component}                   component
     * @param {Element}                     component.dom
     * @param {function((Event|Object))}    component['attrs'].oninput
     */
    oncreate({ dom, 'attrs': { oninput }}){
        let picker = new Pikaday({ //eslint-disable-line no-unused-vars
            ...datePickerConfig,
            'field':    dom,
            'onSelect': function(){
                oninput({ 'target': { 'value': this.getMoment() }});
                m.redraw();
            }
        });
    }

    /**
     * @param {Component}       component
     * @param {Number|Boolean}  component['attrs'].value
     * @param {String}          component['attrs'].placeholder
     * @returns {Component}
     */
    view({ 'attrs': { value, placeholder } }){
        return (
          <input
            className="input datepicker"
            value={ value? moment(value).format('DD/MM/YYYY') : '' }
            placeholder={ placeholder }
        />
      );
    }
}

export default DatePicker;
