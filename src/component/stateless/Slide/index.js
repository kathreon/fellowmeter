/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import './style.sass';

import { SlidePosition } from './SlidePosition';

/**
 * Wrapper Component able to be dynamically animated
 */
export class Slide {
    /**
     * @see https://mithril.js.org/lifecycle-methods.html#onbeforeremove
     * /!\ Time defined in setTimeout and in CSS for animation should be the same
     * @param   {Component}     component
     * @param   {Element}       component.dom
     * @param   {SlidePosition} component.position
     * @returns {Promise}       Resolves at the end of animation
     */
    onbeforeremove({ dom, position}){
        let className = position().getClass(false);
        if(className !== null){
            dom.classList.add(className);
        }
        return new Promise((resolve) => {
            this.timeout = setTimeout(resolve, 500);
        });
    }

    /** @see https://mithril.js.org/lifecycle-methods.html#onremove */
    onremove(){
        clearTimeout(this.timeout);
    }

    /**
     * @param   {Component}         component
     * @param   {Array<Component>}  component.children
     * @param   {SlidePosition}     component.position
     * @returns {Component}
     */
    view({ children, position }){
        let animationClass = position().getClass(true);
        return <div className={'slide' + ((animationClass === null)? '' : ' ' + animationClass)}>
            <div className="container">
                {children}
            </div>
        </div>;
    }
}

/**
 * Container Component for split sub-elements into animated slides.
 * /!\ children have to be Slide Components
 * /!\ You have to specify an arbitrary `key` on each of them
 * @see https://mithril.js.org/keys.html
 * @property {Number}           max
 * @property {SlidePosition}    position
 * @property {function()}       slideRight
 * @property {function()}       slideLeft
 */
export class Slider {
    /**
     * @param {Component}        component
     * @param {Array<Component>} component.children
     */
    constructor({ children }){
        let max = children.length - 1;
        this.position = new SlidePosition({ max });
        this.slideRight = () => {
            this.position = this.position.right();
        };
        this.slideLeft = () => {
            this.position = this.position.left();
        };
    }

    /**
     * @param   {Component} child
     * @returns {Component}
     */
    decorateChild(child){
        child.position = () => this.position;
        return child;
    }

    /**
     * @param   {Component}        component
     * @param   {Array<Component>} component.children
     * @returns {Component}
     */
    view({ children }){
        let active = this.position.active;
        return (
            <div className="level">
                <nav className="level-left">
                    <span onclick={this.slideLeft} className="slide-control">
                        <i className="fa fa-3x fa-chevron-left" />
                    </span>
                </nav>
                <section className="hero is-fullheight">
                    <div className="container">
                        {
                            children
                                .filter((child, index) => ((child.tag === Slide && active === index)))
                                .map((child) => this.decorateChild(child))
                        }
                    </div>
                </section>
                <nav className="level-right">
                    <span onclick={this.slideRight} className="slide-control">
                        <i className="fa fa-3x fa-chevron-right" />
                    </span>
                </nav>
            </div>
        );
    }
}

export default Slide;
