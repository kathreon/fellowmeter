/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

export class SlidePosition {
    constructor({ before = 0, active = 0, max}){
        this.before = before;
        this.active = active;
        this.max = max;
    }
    left(){
        return new SlidePosition({
            'before': this.active,
            'active':   this.previous(),
            'max':      this.max
        });
    }
    right(){
        return new SlidePosition({
            'before': this.active,
            'active':   this.next(),
            'max':      this.max
        });
    }
    next(){
        return this.active >= this.max ? 0 : this.active + 1;
    }
    previous(){
        return this.active <= 0 ? this.max : this.active - 1;
    }
    getClass(isIn){
        switch(this.before){
            case this.previous():
                return isIn? 'is-right-in' : 'is-left-out';
            case this.next():
                return isIn? 'is-left-in' : 'is-right-out';
        }
        return null;
    }
}

export default SlidePosition;
