export class Touchable {
    oncreate({
 dom, 'attrs': {
        ontap = null,
        ondbltap = null,
        ontouchstart = null,
        ontouchmove = null,
        ontouchend = null,
        oncontextmenu = null,
        ontaphold = null,
        ontouchcancel = null,
        onswipeleft = null,
        onswiperight = null,
        onswipeup = null,
        onswipedown = null
    }
}){
        if(ondbltap !== null){
            dom.addEventListener('dbltap', ondbltap, false);
        }
        if(ontap !== null){
            dom.addEventListener('tap', ontap, false);
        }
        if(ontouchstart !== null){
            dom.addEventListener('touchstart', ontouchstart, false);
        }
        if(ontouchmove !== null){
            dom.addEventListener('touchmove', ontouchmove, false);
        }
        if(ontouchend !== null){
            dom.addEventListener('touchend', ontouchend, false);
        }
        if(oncontextmenu !== null){
            dom.addEventListener('contextmenu', oncontextmenu, false);
        }
        if(ontaphold !== null){
            dom.addEventListener('taphold', ontaphold, false);
        }
        if(ontouchcancel !== null){
            dom.addEventListener('touchcancel', ontouchcancel, false);
        }
        if(onswipeleft !== null){
            dom.addEventListener('swipeleft', onswipeleft, false);
        }
        if(onswiperight !== null){
            dom.addEventListener('swiperight', onswiperight, false);
        }
        if(onswipeup !== null){
            dom.addEventListener('swipeup', onswipeup, false);
        }
        if(onswipedown !== null){
            dom.addEventListener('swipedown', onswipedown, false);
        }
    }
    onbeforeremove({
 dom, 'attrs': {
        ontap = null,
        ondbltap = null,
        ontouchstart = null,
        ontouchmove = null,
        ontouchend = null,
        oncontextmenu = null,
        ontaphold = null,
        ontouchcancel = null,
        onswipeleft = null,
        onswiperight = null,
        onswipeup = null,
        onswipedown = null
    }
}){
        if(ondbltap !== null){
            dom.removeEventListener('dbltap', ondbltap, false);
        }
        if(ontap !== null){
            dom.removeEventListener('tap', ontap, false);
        }
        if(ontouchstart !== null){
            dom.removeEventListener('touchstart', ontouchstart, false);
        }
        if(ontouchmove !== null){
            dom.removeEventListener('touchmove', ontouchmove, false);
        }
        if(ontouchend !== null){
            dom.removeEventListener('touchend', ontouchend, false);
        }
        if(oncontextmenu !== null){
            dom.removeEventListener('contextmenu', oncontextmenu, false);
        }
        if(ontaphold !== null){
            dom.removeEventListener('taphold', ontaphold, false);
        }
        if(ontouchcancel !== null){
            dom.removeEventListener('touchcancel', ontouchcancel, false);
        }
        if(onswipeleft !== null){
            dom.removeEventListener('swipeleft', onswipeleft, false);
        }
        if(onswiperight !== null){
            dom.removeEventListener('swiperight', onswiperight, false);
        }
        if(onswipeup !== null){
            dom.removeEventListener('swipeup', onswipeup, false);
        }
        if(onswipedown !== null){
            dom.removeEventListener('swipedown', onswipedown, false);
        }
    }
    view({ children }){
        return children;
    }
}

export default Touchable;

// m.touchHelper = function(options) {
//     return function(element, initialized, context) {
//         if (!initialized) {
//             Object.keys(options).forEach(function(touchType) {
//                 element.addEventListener(touchType, options[touchType], false);
//             });
//             context.onunload = function() {
//                 Object.keys(options).forEach(function(touchType) {
//                     element.removeEventListener(touchType, options[touchType], false);
//                 });
//             };
//         }
//     };
// };
