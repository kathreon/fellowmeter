/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import { If, True } from './If';
import './style/collapse.css';

let id = 0;

export const CollapseLabel = { 'view': ({ children }) => children };
export const CollapseContent = { 'view': ({ children }) => children };

/**
 * Helper Component for make elements able to be spoiled with a button
 * @property {Boolean} collapsed
 * @property {Number} id
 */
export class Collapse {
    /**
     * @param {Component} component
     * @param {Boolean}   component['attrs'].collapsed
     */
    constructor({ 'attrs':{ collapsed = false} }){
        this.collapsed = collapsed;
        this.id = 'collapse' + (++id);
    }

    /**
     * @param   {Component}         component
     * @param   {Array<Component>}  component.children
     * @returns {Component}
     */
    view({ children }){
        return (
          <div className="collapse">
            <input id={ this.id } checked={this.collapsed} type="checkbox" name="tabs" onclick={({ 'target': { checked }}) => {
             this.collapsed = checked;
            }}/>
            <label for={ this.id }>
                { children.filter((child) => child.tag === CollapseLabel) }
            </label>
            <div className="collapse-content">
                <If condition={ this.collapsed }>
                    <True>{ children.filter((child) => child.tag === CollapseContent) }</True>
                </If>
            </div>
        </div>
        );
    }
}

export default Collapse;
