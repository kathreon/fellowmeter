/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import { Markdown } from '../stateless/Markdown';
import { If, True } from '../stateless/If';

/**
 * Component for Contact Locked Mode
 */
export class Locked {
    /**
     * @param {Component} component
     * @param {Contact}   component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': { 'state': { name, title, description, email, mobilePhone, standardPhone, address, context, picture } } }){
        return (
          <div>
            <p className="has-divider">
                <div className="is-divider" data-content="Contact"></div>
                <div className="columns">
                    <div className="column">
                        <If condition={Boolean(picture)}>
                            <True>
                                <p>
                                    <img src={picture}/>
                                </p>
                            </True>
                        </If>
                        <If condition={name.trim() !== ''}>
                            <True>
                                <p>
                                    <i className="fa fa-fw fa-user" />
                                    &nbsp;{ name }
                                </p>
                            </True>
                        </If>
                        <If condition={title.trim() !== ''}>
                            <True>
                                <p>
                                    <i className="fa fa-fw fa-briefcase" />
                                    &nbsp;{ title }
                                </p>
                            </True>
                        </If>
                        <If condition={email.trim() !== ''}>
                            <True>
                                <p>
                                    <i className="fa fa-fw fa-envelope" />
                                    &nbsp;{ email }
                                </p>
                            </True>
                        </If>
                        <If condition={mobilePhone.trim() !== ''}>
                            <True>
                                <p>
                                    <i className="fa fa-fw fa-mobile-phone" />
                                    &nbsp;{ mobilePhone }
                                </p>
                            </True>
                        </If>
                        <If condition={standardPhone.trim() !== ''}>
                            <True>
                                <p>
                                    <i className="fa fa-fw fa-phone" />
                                    &nbsp;{ standardPhone }
                                </p>
                            </True>
                        </If>
                    </div>
                    <div className="column">
                        <If condition={address.trim() !== ''}>
                            <True>
                                <p className="content" >
                                    <Markdown markdown={ address } />
                                </p>
                            </True>
                        </If>
                    </div>
                </div>
            </p>
            <p className="has-divider">
                <div className="is-divider" data-content="Description personnelle"></div>
                <If condition={description.trim() !== ''}>
                    <True>
                        <p className="content">
                            <Markdown markdown={ description } />
                        </p>
                    </True>
                </If>
            </p>
            <p className="has-divider">
                <div className="is-divider" data-content="Description du contexte de travail"></div>
                <If condition={context.trim() !== ''}>
                    <True>
                        <p className="content">
                            <Markdown markdown={ context } />
                        </p>
                    </True>
                </If>
            </p>
        </div>
      );
    }
}
export default Locked;
