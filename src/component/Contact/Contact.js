/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { If, True, False } from '../stateless/If';
import { Locked } from './Locked';
import { Unlocked } from './Unlocked';


/**
 * Contact Main Component
 * @property {GetStateCallback} getState
 */
export class Contact {
    /**
     * @param {Component} component
     * @param {Provider}  component['attrs'].provider
     */
    constructor({ 'attrs': { provider }}){
        this.getState = provider.provide(({ contact = {}, 'application': { locked } }) => ({ ...contact, locked }));
    }

    /**
     * @param {Component} component
     * @param {Actions}   component['attrs'].actions
     * @returns {Component}
     */
    view({ 'attrs': { actions }}){
        let { locked, ...state } = this.getState();
        return (
          <div className="box is-info">
              <div>
                  <If condition={locked}>
                      <True> <Locked state={state} /> </True>
                      <False> <Unlocked state={state} actions={actions} /> </False>
                  </If>
              </div>
          </div>
        );
    }
}

export default Contact;
