/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';

import { Mce } from '../stateless/Mce';
import { Collapse, CollapseContent, CollapseLabel } from '../stateless/Collapse';
import { If, True, False } from '../stateless/If';
import { FileHelper } from '../../lib/FileHelper';
import { RemoveButton } from '../stateless/RemoveButton';

/**
 * Component for Contact Unlocked Mode
 * @property {function(Event)} loadPicture
 * @property {function()} resetPicture
 */
export class Unlocked {
    /**
     * @param {Component} component
     * @param {Actions}   component['attrs'].actions
     */
    constructor({ 'attrs': { actions }}){
        this.loadPicture = ({ target }) => {
            if (target.files.length > 0){
                let file = target.files[0];
                FileHelper.fileToDataUrl(file)
                    .then((dataUrl) => {
                        actions.dispatch(actions.contact.changePicture(dataUrl));
                        m.redraw();
                    });
            }
        };
        this.resetPicture = actions.delegate(actions.contact.resetPicture);
    }

    /**
     * @param {Component} component
     * @param {Actions}   component['attrs'].actions
     * @param {Contact}   component['attrs'].state
     * @returns {Component}
     */
    view({ 'attrs': {actions, 'state': { name, description, email, mobilePhone, standardPhone, address, title, context, picture }}}){
        return (
            <div>
                <p className="has-divider">
                    <div className="is-divider" data-content="Contact"></div>
                    <div className="columns">
                        <div className="column">
                            <div className="field" >
                                <div className="control has-icons-left">
                                    <input
                                        className="input"
                                        oninput={actions.event((value) => actions.contact.changeName(value), true)}
                                        value={ name }
                                        placeholder="Nom complet"
                                    />
                                    <span className="icon is-small is-left">
                                        <i className="fa fa-fw fa-user" />
                                    </span>
                                </div>
                            </div>
                            <div className="field">
                                <div className="control has-icons-left">
                                    <input
                                        className="input"
                                        oninput={actions.event((value) => actions.contact.changeTitle(value), true)}
                                        value={ title }
                                        placeholder="Poste souhaité ou actuel"
                                    />
                                    <span className="icon is-small is-left">
                              <i className="fa fa-fw fa-briefcase" />
                            </span>
                                </div>
                            </div>
                            <div className="field">
                                <div className="control has-icons-left">
                                    <input
                                        className="input"
                                        oninput={actions.event((value) => actions.contact.changeEmail(value))}
                                        value={ email }
                                        placeholder="Email"
                                    />
                                    <span className="icon is-small is-left">
                                        <i className="fa fa-fw fa-envelope" />
                                    </span>
                                </div>
                            </div>
                            <div className="field">
                                <div className="control has-icons-left">
                                    <input
                                        className="input"
                                        oninput={actions.event((value) => actions.contact.changeMobilePhone(value))}
                                        value={ mobilePhone }
                                        placeholder="Téléphone mobile"
                                    />
                                    <span className="icon is-small is-left">
                                        <i className="fa fa-fw fa-mobile-phone" />
                                    </span>
                                </div>
                            </div>
                            <div className="field">
                                <div className="control has-icons-left">
                                    <input
                                        className="input"
                                        oninput={actions.event((value) => actions.contact.changeStandardPhone(value))}
                                        value={ standardPhone }
                                        placeholder="Téléphone fixe"
                                    />
                                    <span className="icon is-small is-left">
                                        <i className="fa fa-fw fa-phone" />
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div className="column">
                            <div className="field">
                                <div className="control">
                                    <Collapse>
                                        <CollapseLabel>
                                            <div className="button is-fullwidth is-medium is-info is-outlined ">Adresse</div>
                                        </CollapseLabel>
                                        <CollapseContent>
                                            <Mce oninput={actions.event((value) => actions.contact.changeAddress(value))}>
                                                { address }
                                            </Mce>
                                        </CollapseContent>
                                    </Collapse>
                                </div>
                            </div>
                            <div className="file is-boxed is-fullwidth">
                                <label className="file-label">
                                    <input className="file-input" onchange={this.loadPicture} type="file"/>
                                    <span className="file-cta">
                                    <If condition={Boolean(picture)}>
                                        <True>
                                            <img src={ picture } />
                                            <RemoveButton onclick={this.resetPicture} />
                                        </True>
                                        <False>
                                              Image de profil
                                              <span className="file-icon">
                                                <i className="fa fa-upload" />
                                              </span>
                                        </False>
                                    </If>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </p>
                <p>
                    <div className="is-divider" data-content="Description personnelle"></div>
                    <div className="field">
                        <div className="control">
                            <Collapse>
                                <CollapseLabel>
                                    <div className="button is-medium is-fullwidth is-info is-outlined">Description</div>
                                </CollapseLabel>
                                <CollapseContent>
                                    <Mce oninput={actions.event((value) => actions.contact.changeDescription(value))}>
                                        { description }
                                    </Mce>
                                </CollapseContent>
                            </Collapse>
                        </div>
                    </div>
                </p>
                <p>
                    <div className="is-divider" data-content="Description du contexte de travail"></div>
                    <div className="field">
                        <div className="control">
                            <Collapse>
                                <CollapseLabel>
                                    <div className="button is-medium is-fullwidth is-info is-outlined is-size-5-mobile">Contexte</div>
                                </CollapseLabel>
                                <CollapseContent>
                                    <Mce oninput={actions.event((value) => actions.contact.changeContext(value))}>
                                        { context }
                                    </Mce>
                                </CollapseContent>
                            </Collapse>
                        </div>
                    </div>
                </p>
            </div>
        );
    }
}
export default Unlocked;
