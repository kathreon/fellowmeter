/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import stub from './stub.json';
import { FileHelper } from '../lib/FileHelper';

/**
 * Raw key in html used to be sure of in-place state replacement
 * @constant
 * @type {string}
 */
const CLONE_KEY = 'cDd4eEcLfSkd0bm9AVURG5PK';

/**
 * Handler for manage window & browser interactions
 * @property    {String}           currentUrl
 * @property    {function(url)}    download
 */
export class Persister {
    /**
     * @param {Object} args
     * @param {Window} args.window
     */
    constructor({ window }){
        this.currentUrl = window.location.href;
        this.download = Persister.downloader(window);
    }

    /**
     * Ask download of a application source pre-loaded with current state
     * @param {Object} state
     * @returns {Promise<Object>}
     */
    downloadStateAsHtml(state){
        return Persister.getPageSourceAsString(this.currentUrl)
            .then(Persister.injector(Persister.serialize(state)))
            .then(this.download('index.html', 'text/html'));
    }

    /**
     * Ask download of a json file which represents the current state of application
     * @param {Object} state
     */
    downloadStateAsJson(state){
        this.download('fellowmeter.json')(JSON.stringify(state));
    }

    /**
     * Extract json configuration of given file
     * @param {File} file
     * @returns {Promise<Object>}
     */
    getFileState(file){
        return FileHelper.fileToText(file)
            .then(JSON.parse);
    }

    /**
     * Extract and deserialize the pre-loaded configuration of current page
     * @param   {Object}    args
     * @param   {Window}    args.window
     * @returns {Object}
     */
    static getInitialState({ window }){
        let deserializedState = Persister.deserialize(Persister.extract(window));
        if(deserializedState === null || typeof deserializedState.application === 'undefined'){
           return stub;
        }
        return deserializedState;
    }

    /**
     * Download source of current url
     * @param {string} url
     * @returns {Promise<String>}
     */
    static getPageSourceAsString(url){
        return m.request({
            'method':  'GET',
            'url':     url,
            'headers': {
                'Content-Type': 'text/plain;charset=utf-8',
                'Accept':       'text/plain;charset=utf-8'
            },
            'extract':    (xhr) => xhr.responseText,
            'background': true,
            'useBody':    false
        });
    }

    /**
     * Extract raw pre-loaded state from current page
     * @param   {Object}    namespace
     * @returns {string}    pre-loaded base64-encoded state
     */
    static extract(namespace){
        return (typeof namespace[CLONE_KEY] === 'undefined')
            ? ''
            : namespace[CLONE_KEY];
    }

    /**
     * @param state
     * @returns {string}
     */
    static serialize(state){
        return btoa(JSON.stringify(state));
    }

    /**
     * @param   {string}    stateAsString
     * @returns {null}
     */
    static deserialize(stateAsString){
        return stateAsString === ''
            ? null
            : JSON.parse(atob(stateAsString));
    }

    /**
     * Replace serialized state into a string with pre-loaded one
     * @callback InjectCallback
     * @param   {string} clonedHtml
     */
    /**
     * @param {Object} serialized
     * @returns {InjectCallback}
     */
    static injector(serialized){
        return (clonedHtml) => {
            let replaceExpr = '<script>(\\s)*var(\\s)*' + CLONE_KEY + '(\\s)*=(\\s)*\'.*\'(\\s)*;(\\s)*<\\/script>';
            return clonedHtml.replace(new RegExp(replaceExpr), () => {
                return '<script>var ' + CLONE_KEY + '=\'' + serialized + '\';</script>';
            });
        };
    }

    /**
     * Ask the user to download a pre-loaded string as file
     * @callback DownloaderCallback
     */
    /**
     * Function for a callback that ask the user to download a file
     * @param   {Object}   args
     * @param   {Document} args.document
     * @returns {DownloaderCallback}
     */
    static downloader({ document }){
        return (filename, mime = 'application/json') => (string) => {
            let fakeLink = document.createElement('a');
            fakeLink.setAttribute('href', 'data:' + mime + ';charset=utf8,' + encodeURIComponent(string));
            fakeLink.setAttribute('download', filename);
            fakeLink.style.display = 'none';
            document.body.appendChild(fakeLink);
            fakeLink.click();
            document.body.removeChild(fakeLink);
        };
    }
}

export default Persister;
