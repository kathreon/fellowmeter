/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 *
 * @param {function()}      reduce
 * @param {Array<Skill>}    state
 * @param {Object}    action
 */
const reduceSkillByCreated = (reduce, state, action) => state.map((skill) => (
    action.createdAt === skill.createdAt
        ? reduce(skill)
        : skill
));

/**
 * @param {Number} createdAt
 * @returns {Skill}
 */
const createNewSkill = (createdAt) => {
    return {
        'createdAt':   createdAt,
        'title':       '',
        'description': '',
        'context':     '',
        'skills':      [],
        'icon':        ''
    };
};


/**
 * Skills Reducer
 * @param {Array<Skill>} state
 * @param {Object}       action
 * @returns {Array<Skill>}
 */
export const skill = (state = [], action) => {
    switch(action.type){
        /** @var {LoadState} action */
        case 'APPLICATION_LOAD_STATE':
            return action.state.skill.map((skillState) => ({ ...skillState }));

        /** @var {ResetAction} action */
        case 'APPLICATION_RESET':
            return [];

        /** @var {SkillAddAction} action */
        case 'SKILL_ADD':
            return [ ...state, createNewSkill(action.createdAt) ];

        /** @var {SkillRemoveAction} action */
        case 'SKILL_REMOVE':
            return state.filter((skillState) => skillState.createdAt !== action.createdAt);

        /** @var {SkillChangeTitleAction} action */
        case 'SKILL_CHANGE_TITLE':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'title': action.title
            }), state, action);

        /** @var {SkillChangeDescriptionAction} action */
        case 'SKILL_CHANGE_DESCRIPTION':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'description': action.description
            }), state, action);

        /** @var {SkillChangeContextAction} action */
        case 'SKILL_CHANGE_CONTEXT':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'context': action.context
            }), state, action);

        /** @var {SkillChangeAddSkillAction} action */
        case 'SKILL_CHANGE_ADD_SKILL':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'skills': [...skillState.skills, action.skillCreatedAt]
            }), state, action);

        /** @var {SkillChangeRemoveSkillAction} action */
        case 'SKILL_CHANGE_REMOVE_SKILL':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'skills': skillState.skills.filter((skillCreateAt) => skillCreateAt !== action.skillCreatedAt)
            }), state, action);

        /** @var {SkillChangePictureAction} action */
        case 'SKILL_CHANGE_PICTURE':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'icon': action.icon
            }), state, action);

        /** @var {SkillResetPictureAction} action */
        case 'SKILL_RESET_PICTURE':
            return reduceSkillByCreated((skillState) => ({
                ...skillState,
                'icon': ''
            }), state, action);
    }
    return state;
};

export default skill;
