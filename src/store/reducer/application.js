/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * Application Reducer
 * @param   {Application}    state
 * @param   {Object}         action
 * @returns {Application}    next state
 */
export const application = (state = {'locked': false, 'selectedSkill': null}, action) => {
    switch(action.type){
        /** @var {LoadStateAction} action */
        case 'APPLICATION_LOAD_STATE':
            return { ...action.state.application };

        /** @var {ResetAction} action */
        case 'APPLICATION_RESET':
            return {
                'locked':        false,
                'selectedSkill': null
            };

        /** @var {LockAction} action */
        case 'APPLICATION_LOCK':
            return {
                ...state,
                'locked': true
            };

        /** @var {UnlockAction} action */
        case 'APPLICATION_UNLOCK':
            return {
                ...state,
                'locked': false
            };

        /** @var {SkillRemoveAction} action */
        case 'SKILL_REMOVE':
            if(state.selectedSkill === action.createdAt){
                return {
                    ...state,
                    'selectedSkill': null
                };
            }
            return state;

        /** @var {SkillAddAction} action */
        case 'SKILL_ADD':
            return {
                ...state,
                'selectedSkill': action.createdAt
            };

        /** @var {SkillSelectAction} action */
        case 'SKILL_SELECT':
            return {
                ...state,
                'selectedSkill': action.createdAt
            };

        /** @var {SkillResetSelectedAction} action */
        case 'SKILL_RESET_SELECTED':
            return {
                ...state,
                'selectedSkill': null
            };
    }
    return state;
};

export default application;
