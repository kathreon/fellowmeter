/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/


/**
 * Contact Reducer
 * @param {Contact} state
 * @param {Object}  action
 * @returns {Contact}
 */
export const contact = (state = {
    'name':          '',
    'description':   '',
    'email':         '',
    'mobilePhone':   '',
    'standardPhone': '',
    'address':       '',
    'title':         '',
    'context':       ''
}, action) => {
    switch(action.type){
        /** @var {LoadStateAction} action */
        case 'APPLICATION_LOAD_STATE':
            return { ...action.state.contact };

        /** @var {ResetAction} action */
        case 'APPLICATION_RESET':
            return {
                'name':          '',
                'description':   '',
                'email':         '',
                'mobilePhone':   '',
                'standardPhone': '',
                'address':       '',
                'title':         '',
                'context':       ''
            };

        /** @var {ChangeContactNameAction} action */
        case 'CHANGE_CONTACT_NAME':
            return {...state, 'name': action.name };

        /** @var {ChangeContactDescriptionAction} action */
        case 'CHANGE_CONTACT_DESCRIPTION':
            return {...state, 'description': action.description };

        /** @var {ChangeContactEmailAction} action */
        case 'CHANGE_CONTACT_EMAIL':
            return {...state, 'email': action.email };

        /** @var {ChangeContactMobilePhoneAction} action */
        case 'CHANGE_CONTACT_MOBILE_PHONE':
            return {...state, 'mobilePhone': action.mobilePhone };

        /** @var {ChangeContactStandardPhoneAction} action */
        case 'CHANGE_CONTACT_STANDARD_PHONE':
            return {...state, 'standardPhone': action.standardPhone };

        /** @var {ChangeContactAddressAction} action */
        case 'CHANGE_CONTACT_ADDRESS':
            return {...state, 'address': action.address };

        /** @var {ChangeContactTitleAction} action */
        case 'CHANGE_CONTACT_TITLE':
            return {...state, 'title': action.title };

        /** @var {ChangeContactContextAction} action */
        case 'CHANGE_CONTACT_CONTEXT':
            return {...state, 'context': action.context };

        /** @var {ChangeContactPictureAction} action */
        case 'CHANGE_CONTACT_PICTURE':
            return {...state, 'picture': action.picture };

        /** @var {ResetContactPictureAction} action */
        case 'RESET_CONTACT_PICTURE':
            return {...state, 'picture': '' };
    }
    return state;
};

export default contact;
