/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import moment from 'moment';

/**
 * @param {function()}  reduce
 * @param {Array<Job>}  state
 * @param {Number}      createdAt
 */
const reduceJobByCreated = (reduce, state, createdAt) => state.map((job) => (
    createdAt === job.createdAt
        ? reduce(job)
        : job
));

/**
 * @param {JobAddAction} action
 * @returns {Job}
 */
const createNewJob = (action) => {
    let now = Number(moment().format('x')),
        from = action.from === null ? now : Number(action.from);
    return {
        'createdAt':   now,
        'title':       '',
        'begin':       from,
        'end':         from + 2000,
        'description': '',
        'context':     '',
        'skills':      [],
        'icon':        ''
    };
};


/**
 * Jobs Reducer
 * @param {Array<Job>} state
 * @param {Object} action
 * @returns {Array<Job>}
 */
export const job = (state = [], action) => {
    switch(action.type){
        /** @var {LoadStateAction} action */
        case 'APPLICATION_LOAD_STATE':
            return action.state.job.map((jobState) => ({ ...jobState }));

        /** @var {ResetAction} action */
        case 'APPLICATION_RESET':
            return [];

        /** @var {JobAddAction} action */
        case 'JOB_ADD':
            return [ createNewJob(action), ...state ];

        /** @var {JobRemoveAction} action */
        case 'JOB_REMOVE':
            return state.filter((jobState) => jobState.createdAt !== action.createdAt);

        /** @var {JobChangeTitleAction} action */
        case 'JOB_CHANGE_TITLE':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'title': action.title
            }), state, action.createdAt);

        /** @var {JobChangeBeginAction} action */
        case 'JOB_CHANGE_BEGIN':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'begin': action.begin
            }), state, action.createdAt);

        /** @var {JobChangeEndAction} action */
        case 'JOB_CHANGE_END':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'end': action.end
            }), state, action.createdAt);

        /** @var {JobChangeDescriptionAction} action */
        case 'JOB_CHANGE_DESCRIPTION':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'description': action.description
            }), state, action.createdAt);

        /** @var {JobChangeContextAction} action */
        case 'JOB_CHANGE_ADD_SKILL':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'skills': [...jobState.skills, action.skillCreatedAt]
            }), state, action.jobCreatedAt);

        /** @var {JobChangeRemoveSkillAction} action */
        case 'JOB_CHANGE_REMOVE_SKILL':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'skills': jobState.skills.filter((skillCreateAt) => skillCreateAt !== action.skillCreatedAt)
            }), state, action.jobCreatedAt);

        /** @var {JobChangeIconAction} action */
        case 'JOB_CHANGE_ICON':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'icon': action.icon
            }), state, action.createdAt);

        /** @var {JobResetIconAction} action */
        case 'JOB_RESET_ICON':
            return reduceJobByCreated((jobState) => ({
                ...jobState,
                'icon': ''
            }), state, action.createdAt);
    }
    return state;
};

export default job;
