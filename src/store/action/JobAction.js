/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * Expose various actions over the job part of the store's state
 * Contains static collection of action generators
 */
export class JobAction {
    /** @typedef {{type: 'JOB_ADD', from: Number}} JobAddAction */
    /**
     * @param   {Number} from
     * @returns {JobAddAction}
     */
    static add(from){
        return {
            'type': 'JOB_ADD',
            from
        };
    }

    /** @typedef {{type: 'JOB_REMOVE', createdAt: Number }} JobRemoveAction */
    /**
     * @param   {Number} createdAt
     * @returns {JobRemoveAction}
     */
    static remove(createdAt){
        return {
            'type': 'JOB_REMOVE',
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_TITLE'}} JobChangeTitleAction */
    /**
     * @param   {String} title
     * @param   {Number} createdAt
     * @returns {JobChangeTitleAction}
     */
    static changeTitle(createdAt, title){
        return {
            'type': 'JOB_CHANGE_TITLE',
            title,
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_BEGIN', title: string, begin: Number, createdAt: Number}} JobChangeBeginAction */
    /**
     * @param   {Number} createdAt
     * @param   {(String|Number)} begin
     * @returns {JobChangeBeginAction}
     */
    static changeBegin(createdAt, begin){
        return {
            'type':  'JOB_CHANGE_BEGIN',
            'begin': Number(begin),
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_END', end: Number, createdAt: Number }} JobChangeEndAction */
    /**
     * @param   {Number} createdAt
     * @param   {(String|Number)} end
     * @returns {JobChangeEndAction}
     */
    static changeEnd(createdAt, end){
        return {
            'type': 'JOB_CHANGE_END',
            'end':  Number(end),
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_DESCRIPTION', description: String, createdAt: Number }} JobChangeDescriptionAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} description
     * @returns {JobChangeDescriptionAction}
     */
    static changeDescription(createdAt, description){
        return {
            'type': 'JOB_CHANGE_DESCRIPTION',
            description,
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_CONTEXT', context: String, createdAt: Number }} JobChangeContextAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} context
     * @returns {JobChangeContextAction}
     */
    static changeContext(createdAt, context){
        return {
            'type': 'JOB_CHANGE_CONTEXT',
            context,
            createdAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_ADD_SKILL', skillCreatedAt: Number, jobCreatedAt: Number }} JobChangeAddSkillAction */
    /**
     * @param   {Number} jobCreatedAt
     * @param   {Number} skillCreatedAt
     * @returns {JobChangeAddSkillAction}
     */
    static changeAddSkill(jobCreatedAt, skillCreatedAt){
        return {
            'type': 'JOB_CHANGE_ADD_SKILL',
            skillCreatedAt,
            jobCreatedAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_REMOVE_SKILL', skillCreatedAt: Number, jobCreatedAt: Number }} JobChangeRemoveSkillAction */
    /**
     * @param   {Number} jobCreatedAt
     * @param   {Number} skillCreatedAt
     * @returns {JobChangeRemoveSkillAction}
     */
    static changeRemoveSkill(jobCreatedAt, skillCreatedAt){
        return {
            'type': 'JOB_CHANGE_REMOVE_SKILL',
            skillCreatedAt,
            jobCreatedAt
        };
    }

    /** @typedef  {{type: 'JOB_CHANGE_REMOVE_SKILL', createdAt: Number, icon: String }} JobChangeIconAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} icon
     * @returns {JobChangeIconAction}
     */
    static changeIcon(createdAt, icon){
        return {
            'type': 'JOB_CHANGE_ICON',
            createdAt,
            icon
        };
    }

    /** @typedef  {{type: 'JOB_RESET_ICON', createdAt: Number }} JobResetIconAction */
    /**
     * @param   {Number} createdAt
     * @returns {JobResetIconAction}
     */
    static resetIcon(createdAt){
        return {
            'type': 'JOB_RESET_ICON',
            createdAt
        };
    }
}

export default JobAction;
