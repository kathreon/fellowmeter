/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * Expose various actions over the application part of the store's state
 * Contains static collection of action generators
 */
export class ApplicationAction {
    /** @typedef {{type: 'APPLICATION_LOCK'}} LockAction */
    /** @returns {LockAction} */
    static lock(){
        return { 'type': 'APPLICATION_LOCK' };
    }

    /** @typedef {{type: 'APPLICATION_UNLOCK'}} UnlockAction */
    /** @returns {UnlockAction} */
    static unlock(){
        return { 'type': 'APPLICATION_UNLOCK' };
    }

    /** @typedef {{type: 'APPLICATION_RESET'}} ResetAction */
    /** @returns {ResetAction} */
    static reset(){
        return { 'type': 'APPLICATION_RESET' };
    }

    /** @typedef {{type: 'APPLICATION_LOAD_STATE', state: Object}} LoadStateAction */
    /**
     * @param {Object} state
     * @returns {LoadStateAction}
     **/
    static loadState(state){
        return { 'type': 'APPLICATION_LOAD_STATE', state };
    }
}

export default ApplicationAction;
