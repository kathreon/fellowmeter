/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * Expose various actions over the contact part of the store's state
 * Contains static collection of action generators
 */
export class ContactAction {
    /** @typedef {{type: 'CHANGE_CONTACT_NAME', name: string}} ChangeContactNameAction */
    /**
     * @param   {String} name
     * @returns {ChangeContactNameAction}
     */
    static changeName(name){
        return {
            'type': 'CHANGE_CONTACT_NAME',
            name
        };
    }

    /** @typedef {{type: 'CHANGE_CONTACT_DESCRIPTION', description: string}} ChangeContactDescriptionAction */
    /**
     * @param   {String} description
     * @returns {ChangeContactDescriptionAction}
     */
    static changeDescription(description){
        return {
            'type': 'CHANGE_CONTACT_DESCRIPTION',
            description
        };
    }

    /** @typedef {{type: 'CHANGE_CONTACT_EMAIL', email: string}} ChangeContactEmailAction */
    /**
     * @param   {String} email
     * @returns {ChangeContactEmailAction}
     */
    static changeEmail(email){
        return {
            'type': 'CHANGE_CONTACT_EMAIL',
            email
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_MOBILE_PHONE', mobilePhone: string}} ChangeContactMobilePhoneAction */
    /**
     * @param {String} mobilePhone
     * @returns {ChangeContactMobilePhoneAction}
     */
    static changeMobilePhone(mobilePhone){
        return {
            'type': 'CHANGE_CONTACT_MOBILE_PHONE',
            mobilePhone
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_STANDARD_PHONE', standardPhone: string}} ChangeContactStandardPhoneAction */
    /**
     * @param {String} standardPhone
     * @returns {ChangeContactStandardPhoneAction}
     */
    static changeStandardPhone(standardPhone){
        return {
            'type': 'CHANGE_CONTACT_STANDARD_PHONE',
            standardPhone
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_ADDRESS', address: string}} ChangeContactAddressAction */
    /**
     * @param {String} address
     * @returns {ChangeContactAddressAction}
     */
    static changeAddress(address){
        return {
            'type': 'CHANGE_CONTACT_ADDRESS',
            address
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_TITLE', title: string}} ChangeContactTitleAction */
    /**
     * @param {String} title
     * @returns {ChangeContactTitleAction}
     */
    static changeTitle(title){
        return {
            'type': 'CHANGE_CONTACT_TITLE',
            title
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_CONTEXT', context: string}} ChangeContactContextAction */
    /**
     * @param {String} context
     * @returns {ChangeContactContextAction}
     */
    static changeContext(context){
        return {
            'type': 'CHANGE_CONTACT_CONTEXT',
            context
        };
    }

    /** @typedef  {{type: 'CHANGE_CONTACT_PICTURE'}} ChangeContactPictureAction */
    /**
     * @param   {String} picture
     * @returns {ChangeContactPictureAction}
     */
    static changePicture(picture){
        return {
            'type': 'CHANGE_CONTACT_PICTURE',
            picture
        };
    }

    /** @typedef {{type: 'RESET_CONTACT_PICTURE'}} ResetContactPictureAction */
    /** @returns {ResetContactPictureAction} */
    static resetPicture(){
        return { 'type': 'RESET_CONTACT_PICTURE' };
    }
}

export default ContactAction;
