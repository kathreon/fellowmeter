/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import moment from 'moment';

/**
 * Expose various actions over the contact part of the store's state
 * Contains static collection of action generators
 */
export class SkillAction {
    /** @typedef  {{type: 'SKILL_SELECT', createdAt: Number }} SkillSelectAction */
    /**
     * @param   {Number} createdAt
     * @returns {SkillSelectAction}
     */
    static select(createdAt){
        return {
            'type': 'SKILL_SELECT',
            createdAt
        };
    }

    /** @typedef  {{type: 'SKILL_RESET_SELECTED' }} SkillResetSelectedAction */
    /** @returns {SkillResetSelectedAction} */
    static reset(){
        return {'type': 'SKILL_RESET_SELECTED'};
    }

    /** @typedef  {{type: 'SKILL_ADD', createdAt: Number }} SkillAddAction */
    /** @returns {SkillAddAction} */
    static add(){
        return {'type': 'SKILL_ADD', 'createdAt': Number(moment().format('x')) };
    }

    /** @typedef  {{type: 'SKILL_REMOVE', createdAt: Number }} SkillRemoveAction */
    /** @returns {SkillRemoveAction} */
    static remove(createdAt){
        return {
            'type': 'SKILL_REMOVE',
            createdAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_TITLE', createdAt: Number }} SkillChangeTitleAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} title
     * @returns {SkillChangeTitleAction}
     */
    static changeTitle(createdAt, title){
        return {
            'type': 'SKILL_CHANGE_TITLE',
            title,
            createdAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_DESCRIPTION', createdAt: Number, description: String }} SkillChangeDescriptionAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} description
     * @returns {SkillChangeDescriptionAction}
     */
    static changeDescription(createdAt, description){
        return {
            'type': 'SKILL_CHANGE_DESCRIPTION',
            description,
            createdAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_CONTEXT', createdAt: Number, context: String }} SkillChangeContextAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} context
     * @returns {SkillChangeContextAction}
     */
    static changeContext(createdAt, context){
        return {
            'type': 'SKILL_CHANGE_CONTEXT',
            context,
            createdAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_ADD_SKILL', createdAt: Number, skillCreatedAt: Number }} SkillChangeAddSkillAction */
    /**
     * @param   {Number} createdAt
     * @param   {Number} skillCreatedAt
     * @returns {SkillChangeAddSkillAction}
     */
    static changeAddSkill(createdAt, skillCreatedAt){
        return {
            'type': 'SKILL_CHANGE_ADD_SKILL',
            createdAt,
            skillCreatedAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_REMOVE_SKILL', createdAt: Number, skillCreatedAt: Number }} SkillChangeRemoveSkillAction */
    /**
     * @param   {Number} createdAt
     * @param   {Number} skillCreatedAt
     * @returns {SkillChangeRemoveSkillAction}
     */
    static changeRemoveSkill(createdAt, skillCreatedAt){
        return {
            'type': 'SKILL_CHANGE_REMOVE_SKILL',
            createdAt,
            skillCreatedAt
        };
    }

    /** @typedef  {{type: 'SKILL_CHANGE_PICTURE', createdAt: Number, icon: String }} SkillChangePictureAction */
    /**
     * @param   {Number} createdAt
     * @param   {String} icon
     * @returns {SkillChangePictureAction}
     */
    static changeIcon(createdAt, icon){
        return {
            'type': 'SKILL_CHANGE_PICTURE',
            createdAt,
            icon
        };
    }

    /** @typedef  {{type: 'SKILL_RESET_PICTURE', createdAt: Number }} SkillResetPictureAction */
    /**
     * @param   {Number} createdAt
     * @returns {SkillResetPictureAction}
     */
    static resetIcon(createdAt){
        return {
            'type': 'SKILL_RESET_PICTURE',
            createdAt
        };
    }
}

export default SkillAction;
