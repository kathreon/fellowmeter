/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import { createStore, combineReducers, applyMiddleware } from 'redux';
import { application } from './reducer/application';
import { contact } from './reducer/contact';
import { skill } from './reducer/skill';
import { job } from './reducer/job';

/**
 * Redux middleware for logging action & store previous/next state
 * @type Middleware
 * @param {S} store
 */
const logger = (store) => (next) => (action) => {
    /* eslint-disable no-console */
    console.group(action.type);
    console.info('dispatching', action);
    let result = next(action);
    console.log('next state', store.getState());
    console.groupEnd();
    return result;
    /* eslint-enable no-console */
};

/**
 * Manager access to the Store
 */
export class Provider {
    /**
     * @param {Object}  args
     * @param {State}   args.preload
     */
    constructor({ preload }){
        this.store = createStore(combineReducers({
            application,
            contact,
            skill,
            job
        }), preload, applyMiddleware(logger));
    }

    /**
     * Filter full state to a expose a custom piece of store
     * @callback SelectStateCallback
     * @param   {State} state
     * @returns {Object} selectedState
     */
    /**
     * Return state with pre-loaded filter method
     * @callback GetStateCallback
     * @returns {Object} selectedState
     */
    /**
     * Functor that provide a way to access to a custom piece of store
     * @param   {SelectStateCallback} select
     * @returns {GetStateCallback}
     */
    provide(select){
        return () => select(this.getState());
    }

    /**
     * Give the current store's state
     * @returns {State}
     */
    getState(){
        return this.store.getState();
    }
}

export default Provider;
