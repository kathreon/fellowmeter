/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import { ApplicationAction } from './action/ApplicationAction';
import { ContactAction } from './action/ContactAction';
import { SkillAction } from './action/SkillAction';
import { JobAction } from './action/JobAction';

/**
 * An ActionGeneratorCallback is a function which generate an action object
 * @callback ActionGeneratorCallback
 * @return  {Object}    action object
 */
 /**
 * An ActionDispatcherCallback is a function which dispatch a pre-filled generated action through the store
 * @callback ActionDispatcherCallback
 * @return  {Object}    Next state
 */
 /**
 * Manage actions dispatching over the store
 * Should be given to any Components which has to perform write operations
 * @property    {function()}        storeDispatch
 * @property    {ApplicationAction} application
 * @property    {SkillAction}       skill
 * @property    {ContactAction}     contact
 * @property    {JobAction}         job
 */
export class Actions {
    /**
     * @param {Object} args
     * @param {function()} args.dispatch
     */
    constructor({ dispatch }){
        this.storeDispatch = dispatch;
        this.application = ApplicationAction;
        this.skill = SkillAction;
        this.contact = ContactAction;
        this.job = JobAction;
    }

    /**
     * Dispatch an action to the store
     * @param   action      Action to dispatch
     * @returns {Object}    Next state
     */
    dispatch(action){
        return this.storeDispatch(action);
    }

    /**
     * Syntactic sugar helper for generating event handlers without parameters
     * @param   {ActionGeneratorCallback}   actionGenerator
     * @returns {ActionGeneratorCallback}
     */
    delegate(actionGenerator){
        return () => this.storeDispatch(actionGenerator.apply(null, Array.prototype.slice.call(arguments)));
    }

    /**
     * Syntactic sugar  helper for generating event handlers with parameters
     * @param {ActionGeneratorCallback}     actionGenerator
     * @param {Boolean} redraw  Force a mithril redraw when the event is triggered
     * @returns {ActionDispatcherCallback}
     */
    event(actionGenerator, redraw = false){
        return (event) => {
            event.redraw = redraw;
            let { 'target': { value }} = event;
            this.storeDispatch(actionGenerator.apply(actionGenerator, [
                    value,
                    ...Array.prototype.slice.call(arguments)
                ]
            ));
        };
    }
}
export default Actions;
