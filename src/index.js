/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

import m from 'mithril';
import { Application } from './component/Application/Application';
import { Provider } from './store/Provider';
import { Actions } from './store/Actions';
import {Persister} from './store/Persister';

(function(){
    function app(){
        const cloneKey = 'cDd4eEcLfSkd0bm9AVURG5PK';
        let persister = new Persister({ window });
        let provider = new Provider({ 'preload': Persister.getInitialState({ window }) });
        let actions = new Actions(provider.store);
        m.mount(document.body, () => new Application({ 'attrs': { provider, actions, persister, cloneKey } }));
    }

    app();
})();
