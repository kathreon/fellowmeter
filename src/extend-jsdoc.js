/**
 * @see https://mithril.js.org/components.html
 * @typedef  {Object} Component
 * @property {Element}                  dom
 * @property {Array<Component>)         children
 * @property {String|Class|function(*)} tag
 * @property {Number|String}            key
 * @property {String}                   text
 * @property {Object}                   attrs
 */

/**
 * @typedef  {Object} Application
 * @property {Boolean}          locked
 * @property {(Number|null)}    selectedSill
 */

/**
 * @typedef  {Object} Contact
 * @property {String} name
 * @property {String} description   - markdown
 * @property {String} email
 * @property {String} mobilePhone
 * @property {String} standardPhone
 * @property {String} website
 * @property {String} address       - markdown
 * @property {String} title
 * @property {String} context       - markdown
 * @property {String} picture       - base64 / data-url encoded image
 */

/**
 * @typedef  {Object} Skill
 * @property {Number} createdAt     - JS timestamp
 * @property {String} title
 * @property {String} description   - markdown
 * @property {String} context       - markdown
 * @property {Array<Number>} skills - JS timestamps
 * @property {String} picture       - base64 / data-url encoded image
 */

/**
 * @typedef  {Object} Job
 * @property {Number} createdAt     - JS timestamp
 * @property {String} title
 * @property {Number} begin         - JS timestamp
 * @property {Number} end           - JS timestamp
 * @property {String} description   - markdown
 * @property {String} context       - markdown
 * @property {Array<Number>} skills - JS timestamps
 * @property {String} icon          - base64 / data-url encoded image
 */

/**
 * @typedef {Object} State
 * @property {Application}  application
 * @property {Contact}      contact
 * @property {Array<Skill>} skill
 * @property {Array<Job>}   job
 */
