/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2017 François Cadeillan
*/

/**
 * Helper for extract files into various format
 */
export class FileHelper {
    /**
     * @param {File} file
     * @returns {Promise<String>}
     */
    static fileToText(file){
        let reader = new FileReader();

        return new Promise((resolve, reject) => {
            reader.addEventListener('load', ({ target }) => {
                if(target.error){
                    reject(target.error);
                    return false;
                }
                resolve(reader.result);
            });
            reader.readAsText(file);
        });
    }

    /**
     * @param {File} file
     * @returns {Promise<String>}
     */
    static fileToDataUrl(file){
        let reader = new FileReader();

        return new Promise((resolve, reject) => {
            reader.addEventListener('load', ({ target }) => {
                if(target.error){
                    reject(target.error);
                    return false;
                }
                resolve(reader.result);
            });
            reader.readAsDataURL(file);
        });
    }
}

export default FileHelper;
