export const CustomEventPolyfill = ({ CustomEvent, Event, 'document': { createEvent } }) => {
    if (CustomEvent){
        return CustomEvent;
    }
    CustomEvent = function(event, params){
        params = params || { 'bubbles': false, 'cancelable': false, 'detail': undefined }; // eslint-disable-line no-undefined
        let evt = createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    };
    CustomEvent.prototype = Event.prototype;
    return CustomEvent;
};

export default CustomEventPolyfill;
