# The FellowMeter project

## TL;DR

* Production build : `yarn build`

* Development build : `yarn build-dev`

* Development Watch : `yarn watch`

* Check code style : `yarn linter`

* Generate documentation : `yarn doc`

A new html file will be created in the dist directory

## Introduction

This project is a tool for the use of numeric creations teams.

It is intended to be a way of having a better understanding of their
teammates skills.

## Constraints

This application have to be deployed as a single html file without any computing server behind.

It could be served through any file protocol, including HTTP.


## Usage

Open the html file that is located in the dist directory with a web browser.

> App will be in edition mode by default.

Then, add informations, select or upload icons.

> You can preview your work by clicking onto the "lock" button then come back to
edition mode with the tiny "unlock" button.

For persisting your work, click on the save button in order to download your profile json file which describe all your
informations.

Later, you can edit a profile json file by clicking on the "load" button.

If you think your "fellow" profile is done, ensure is it loaded then click on the "generate" : it will makes you download the same
HTML application but in "locked" mode with your data preloaded into.

Then do whatever you want with your preloaded application : send it by mail to your teammates, push it into an HTTP server.

## Behaviors

### Component Unlock

No display if lock is off

button.unloack.onclick => unlock mode

### Component Menu

No display if lock is on

button.save.onclick => send index.html preloaded by store
button.load.onclick => open file load && replace store
button.lock.onclick => set lock mode
button.help.onclick => open help

### Component Help

### Component Modal?

### Component Description

No edit button if lock is on

button.edit.onclick => open descriptionform

### Component DescriptionForm

descriptionform.submit => store description.data
descriptionform.button.exit.onclick => close modal
descriptionform.input[type=text].name => description.data.name
descriptionform.input[type=tinymce].description => description.description

### Component Contact

No edit button if lock is on

button.edit.onclick => open contactform

#### Component ContactForm

contactform.submit => store contactform.data
contactform.button.exit.onclick => close modal
contactform.input[type=file].image => contactform.data.image
contactform.input[type=text].phone_mobile =>  contactform.data.phone.mobile
contactform.input[type=text].phone_standard =>  contactform.data.phone.standard
contactform.input[type=text].website_title =>  contactform.data.website.title
contactform.input[type=text].website_link => contactform.data.website.link
contactform.input[type=text].address => contactform.data.address


### Component Detail

No edit button if lock is on

button.edit.onclick => open detailform
box.job[name].onclick => load job details
box.skill[name].onclick => load skill details

### Component DetailForm

detailform.submit => store detailform.data
detailform.button.exit.onclick => close modal
detailform.input[type=text].title => detailform.title
detailform.input[type=tinymce].jobdescription => detailform.jobdescription
detailform.input[type=tinymce].personaldescription => detailform.personaldescription

### Component JobList

No add, edit, delete button if lock is on

button.add.onclick => open jobform without data
box.job[name].edit.onclick => open jobform with data preload
box.job[name].delete.onclick => open deleteform loaded with delete action

### Component SkillList

No add, edit, delete button if lock is on

button.add.onclick => skillform without data
box.skill[name].edit.onclick => open skillform with data preload
box.skill[name].delete.onclick => open deleteform loaded with delete action

### Component Deleteform

deleteform.button.yes.onclick => send delete action
deleteform.button.no.onclick => close modal

### Component JobForm

jobform.submit => store jobform.data
jobform.button.exit.onclick => close modal
jobform.input[type=file].title => jobform.title
jobform.input[type=file].begin => jobform.begin
jobform.input[type=file].end => jobform.end
jobform.input[type=tinymce].description => jobform.description
jobform.input[type=tinymce].context => jobform.context

### Component SkillForm

skillform.submit => store skillform.data
skillform.button.exit.onclick => close modal
skillform.input[type=file].image => skillform.data.image
skillform.input[type=text].label => skillform.data.label
skillform.input[type=tinymce].description => skillform.data.description
skillform.input[type=tinymce].context => skillform.data.context
skillform.input[type=select].skills => skillform.data.skills
skillform.input[type=select].jobs => skillform.data.jobs
