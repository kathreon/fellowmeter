---

# TODOS
  

* Find the best way to build index.html app as wanted [OK]
  * split up development & production workflow ? [OK]
  * https://stackoverflow.com/questions/33558396/gulp-webpack-or-just-webpack
  * https://www.keithcirkel.co.uk/why-we-should-stop-using-grunt/
  * https://github.com/jharris4/html-webpack-include-assets-plugin
  * https://github.com/markdalgleish/static-site-generator-webpack-plugin

* Webpack
  * Compile es6 javascript [OK]
    * embbed es5 javascript & dependencies [OK]
  * Compile SASS & bulma https://github.com/stipsan/bulma-loader [OK]
    * embbed css [OK]
  * Encode fonts & font-awesome [OK]
    * embbed all fonts formats data-url [OK]
  * Encode images [OK]
    * embbed data-url [OK]
  * Embbed SVG directly [OK]
  * production optimization
    * tree-shaking
    * uglification [OK]
  * add hinter / linter
    * reuse eslint6 file from openmew
    * break compilation if file ain't valid
  * add mocha / chai tests
    * break compilation if tests doesn't passed


* application
    * handle tinyMCE / Ace or another wysiwyg editor [OK]
    * split mce [OK] & pikaday into global statelesses [OK]
    * animations
    * spoiler pour les textareas [OK]
    * clear events of mce with mithril onremove [OK]
    * clear events of pikaday with mithril onremove [OK]
    * tiny lib for render
        * handle scrollTop
    * handle reset application
    * ajouter un loader pour les > 4Mo en remote (+ say to webpack 4Mo is big )
    * add last modification date
    * handle load json
    * handle save json
    * handle cloning
    * handle touch
    * handle file upload
        * for skills
        * for jobs
        * for contactInformation
    * cuteness
        * add colors everywhere
        * Perform a brand new integration without bootstrap
        * create ux design sketch
    * responsiveness
        * handle page handling according to viewport
        * proper responsive menu
    * Enforce json-schema validation
        * create pure entity objects ?
        * tcomb ?
    * make kind of pure repository for storing view sort / filters
    * Theme stateless able to wrap theme parts dynamically
    * Page handling - split the page in 3 page ( think if we need specific onboard )
    * faire un historique ( desactivable au clonage )
    * backlog : prévoir la possibilité de clonage dans une version allégée mais dépendante d'internet ( cdn ). de cette façon on pourrais proposer des set de contenus préloadés

* jobs
    * job deletion [OK]
    * job edition [OK]
    * job deletion [OK]
    * job / skill filtering [OK]
    * job / skill reset [OK]
    * job skill multiselect field

* skills
    * skill edition [OK]
    * skill adding [OK]
    * skill show [OK]
    * skill glowing [OK]
    * skill selection [OK]
    * skill deletion [OK]
    * skill select other skills
