/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2016 François Cadeillan
*/

# Success story

## Role publicateur

* tu accède a l'application
* tu édites tes contact informations ( + définition du poste souhaité / occupé )
* cliques sur le bouton droite ( qui est rouge vu qu'il y a pas de skills )
* tu édites tes skills
* cliques sur le bouton droite ( qui est rouge vu qu'il y a pas de jobs qui est présent vu que l'app est delockée )
* tu édites les jobs
* tu lock pour preview ( et tu regardes le rendu en navigant entre les 3 pages cf Role visionneur )
* sauvegarde dans le menu

## Role visionneur

* tu accède a l'application
* tu consulte les contact informations ( + définition du poste souhaité / occupé )
* cliques sur le bouton droite ( qui est vert vu qu'il y a au moins un skill )
* tu consultes les skills
* tu selectionnes un skill ( les skills associés sont "illuminés" )
* tu cliques sur le bouton droite ( qui est vert car il y a des jobs et présent vu qu'il y a des jobs et que l'app est lockée )
* tu consultes uniquement les jobs associés au skill selectionné
* tu peux reset les jobs pour les voir tous
* tu peux retourner sur les skills avec bouton gauche pour en selectionner un autre


## Menu

* reset l'application

* sauvegarder l'état
* preloader l'état
* cloner
* locker / delocker


## Tasks ( draft )

* upload d'image
* clonage
* loading / sauvegarde config
* switching d'écran
* responsiveness
