/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2016 François Cadeillan
*/

Cet outil va permettre de :
- générer un cv à partir d'une conf json
- de manière responsive

Les objets :
User
SkillCategory
Skill
Job
// ns.Conf = function(){
//   // this.url = m.prop('/conf.json');
//   this.data = m.prop({'skill':[],'job':[]/*{
//     'skill':[
//       {
//         'name': 'PHP',
//         'acronymof' : 'PHP Hypertext Preprocessor',
//         'version':['5','7']
//       },
//       {
//         'name': 'Javascript',
//         'version':['ECS5','ECS6']
//       },
//       {
//         'name': 'jQuery',
//         'acronymof': 'Javascript Query',
//         'slogan': 'Write less, do more',
//         'version':['2.1.x','2.2.x'],
//         'parent':'javascript'
//       },
//       {
//         'name': 'CSS',
//         'acronymof': 'Cascading Style Sheet',
//         'version':['2','3']
//       }
//     ],
//     'job':[
//       {
//         'relatedskill':['php','javascript','jquery']
//       }
//     ]
//   }*/);
// };

revert a file with git

function revert(){
  file=$1
  git checkout $(git rev-list -n 1 HEAD -- "$file")^ -- "$file"
}
