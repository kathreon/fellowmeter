/*This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

If it is not possible or desirable to put the notice in a particular
file, then You may include the notice in a location (such as a LICENSE
file in a relevant directory) where a recipient would be likely to look
for such a notice.

Copyright 2016 François Cadeillan
*/
/*TINYMCE*/
  function get_conf(selector){
    return {
      selector: selector+' textarea.mce',
      theme:'modern',
      menubar : false,
      contextmenu: "paste link image inserttable | cell row column deletetable",
      language: 'fr',
      plugins: ["autoresize code advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "wordcount visualblocks visualchars code fullscreen",
          "insertdatetime nonbreaking save table media contextmenu directionality",
          "template paste textcolor colorpicker textpattern"],
      toolbar1: "undo redo paste code | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | fullscreen | removeformat ",
      paste_retain_style_properties: "all",
      paste_webkit_styles:"all",
    };
  }
  /*
    This code is for demo purpose only - it mean you shouldn't integrate it to your project as is.
    It lazy load tinymce editors.
    Notice it do not handle startup opened modal.
    May not work with old browsers.
  */

  var links = document.querySelectorAll('a.popup-link');
  var loaded = [];
  for(var i = 0; i < links.length; i++){
    (function(link){
      link.addEventListener('click', function(){
        var href = link.getAttribute('href');
        if(loaded.indexOf(href) === -1){
          tinymce.init(get_conf(href));
          loaded.push(href);
        }
      });
    })(links[i]);
  }

/*PIKADAY*/
var pickers = document.querySelectorAll('input.datepicker');
for(var i = 0; i < links.length; i++){
  (function(picker){
    new Pikaday({
      field: picker,
      maxDate: new Date(),
      // onSelect:function(){
      //   picker.setAttribute('value', this.getMoment().format('DD/MM/YYYY'));
      // },
      // format: 'DD/MM/YYYY',
    });
  })(pickers[i]);;
}
