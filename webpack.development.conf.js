const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractCss =   new ExtractTextPlugin({
    'filename': '[name].css'
});

module.exports = {
  'entry': {
    'app': './src/index.js'
  },
  'resolve': {
    'alias': {
      'font-awesome': path.resolve(__dirname, 'node_modules/font-awesome')
    }
  },
  'watchOptions': {
    'poll': true
  },
  'output': {
    'path': path.resolve(__dirname, 'dist'),
    'filename': '[name].js',
  },
  'devtool': 'inline-source-map ',
  'stats': {
    'colors': true,
    'modules': true,
    'reasons': true,
    'errorDetails': true
  },
  'module': {
    'loaders': [],
    'rules': [
      {
        'test': /\.(sass|css)$/,
        'use': extractCss.extract({
            'use': [{
                'loader': "css-loader"
            }, {
                'loader': "sass-loader",
                'options': {
                    'includePaths': ["node_modules"]
                }
            }
          ],
          // use style-loader in development
          'fallback': "style-loader"
        })
      },
      {
        'test': /\.(png|jpg|jpeg|eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
        'use': 'url-loader'
      },
      {
        'test': /\.(js|jsx)$/,
        'exclude': /(node_modules|bower_components)/,
        'use': {
          'loader': 'babel-loader',
          'options': {
            'presets': ['env'],
            'plugins': [
              ["transform-react-jsx", {
                "pragma": "m"
              }],
              ["transform-object-rest-spread"]
            ]
          }
        }
      },
    ]
  },
  'plugins': [
    extractCss,
    new HtmlWebpackPlugin({
        'title': 'FellowMeter',
        'template': 'src/index.html',
        'xhtml': true,
        'inlineSource': '.(js|css)$', // embed all javascript and css inline,
    }),
    new HtmlWebpackInlineSourcePlugin()
  ]
};
